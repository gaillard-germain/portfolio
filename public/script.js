window.onscroll = function() {stickyHeader()};

var header = document.getElementById("index-header");

var sticky = header.offsetTop;


function stickyHeader() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}


window.onclick = function(event) {
  if (!event.target.matches('.dropdown i')) {
    if ($('.dropdown-content').is(':visible')) {
      toggleDropdown();
    }
  }
}


function toggleDropdown() {
  $('.dropdown-content').toggle("medium");
  $('#dropdown-button').toggleClass("fa-caret-square-down fa-caret-square-up");
};


$('#dropdown-button').on('click', function(event) {
  event.stopPropagation();
  event.preventDefault();
  toggleDropdown();
});


$('.tile a').hover(
  function() {
    $(this).parent().find('h2').css('border-color', 'var(--orange)');
  },
  function() {
    $(this).parent().find('h2').css('border-color', 'transparent');
  }
);
